package com.tiago.util;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class DateUtilTest {

  @Rule
  public ExpectedException expectedEx = ExpectedException.none();
  
  @Test
  public void whenDateParameterIsNull_thenThrowIllegalArgumentException() {
    expectedEx.expect(IllegalArgumentException.class);
    expectedEx.expectMessage("date parameter is null");
    
    DateUtil.parse(null);
  }
  
  @Test
  public void whenDateParameterIsInvalid_thenThrowRuntimeException() {
    String invalidDateStr = "2019-01-a";
    
    expectedEx.expect(RuntimeException.class);
    expectedEx.expectMessage("\"" + invalidDateStr + "\" is not a valid date");
    
    DateUtil.parse(invalidDateStr);
  }
  
  @Test
  public void whenDateParameterIsValid_thenReturnLocalDateTimeObject() {
    String validDateTimeStr = "2019-01-01.22:03:01";
    String returneDateTimeStr = DateUtil.parse(validDateTimeStr).format(DateUtil.FORMATTER);
    
    assertEquals(validDateTimeStr, returneDateTimeStr);
  }
}
