package com.tiago.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.stereotype.Component;

/**
 * Utility class that deals with dates.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Component
public class DateUtil {
  
  public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss");
  
  /**
   * Parse a String to a LocalDateTime object.
   * 
   * @param date the String in "yyyy-MM-dd.HH:mm:ss" format
   * @return LocalDateTime
   * @throws IllegalArgumentException if date parameter is null
   * @throws DateTimeParseException if date parameter is in invalid format
   */
  public static LocalDateTime parse(String date) {
    if(date == null) {
      throw new IllegalArgumentException("date parameter is null");
    }
    
    LocalDateTime ldt = null;
    
    try {
      ldt = LocalDateTime.parse(date, FORMATTER); 
    } catch (DateTimeParseException e) {
      throw new RuntimeException("\"" + date + "\" is not a valid date");
    }
    
    return ldt;
  }
}
