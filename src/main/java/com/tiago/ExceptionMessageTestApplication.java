package com.tiago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExceptionMessageTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExceptionMessageTestApplication.class, args);
	}

}

